const { knex } = require('../config/db');

class UserModel {

    static save(data){
        return knex
        .from('users')
        .insert(data);
    }

    static get(id){
        return knex
        .from('users')
        .where('users.id', id);
    }

    static auth(email){
        return knex('users')
        .select({
            email: 'email',
            password: 'password',
            id: 'id'
        })
        .where('email', email);
    }

    static getUsers(id){
        return knex('users')
        .whereNot('id', id);
    }

    static update(id, amount, operation){
        return operation == '+'
        ?
            knex('users')
            .where('id', id)
            .increment('balance', amount)
        :
            knex('users')
            .where('id', id)
            .decrement('balance', amount);
    }

    static getBalance(id){

        return knex('users')
        .select('balance')
        .where('id', id);

    }

}

module.exports = UserModel;