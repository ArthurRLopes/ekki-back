const { knex } = require('../config/db');

class ContactModel {

    static getByUser(id){
        return knex('contacts')
        .where('idAccount', id);
    }

    static get(id){
        return knex('contacts')
        .where('id', id);
    }

    static save(data){
        return knex('contacts')
        .insert(data);
    }

    static update(id, data){
        return knex('contacts')
        .where('id', id)
        .update(data);
    }

    static delete(id){
        return knex('contacts')
        .where('id', id)
        .del();
    }

}

module.exports = ContactModel;