const { knex } = require('../config/db');

class CreditCardModel {

    static getByAccount(id){
        return knex('creditCards')
        .where('idAccount', id);
    }

    static get(id){
        return knex('creditCards')
        .where('id', id);
    }

    static update(id, data){
        return knex('creditCards')
        .where('id', id)
        .update(data);
    }

    static save(data){
        return knex
        .from('creditCards')
        .insert(data);
    }

    static delete(id){
        return knex('creditCards')
        .where('id', id)
        .del();
    }

}

module.exports = CreditCardModel;