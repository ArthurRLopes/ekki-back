const { knex } = require('../config/db');

class AccountModel {

    static save(data){
        return knex('accounts')
        .insert(data);
    }

    static get(id){
        return knex('accounts')
        .where('id', id);
    }

    static getByUser(id){
        return knex('accounts')
        .where('userId', id);
    }
    
    static update(id, data){
        return data.operation == '+' 
        ?
            knex('accounts')
            .where('id', id)
            .increment('balance', data.amount)
        :
            knex('accounts')
            .where('id', id)
            .decrement('balance', data.amount);

    }

    static delete(id){
        return knex('accounts')
        .where('id', id)
        .del();
    }

}

module.exports = AccountModel;