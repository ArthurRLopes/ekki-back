const { knex } = require('../config/db');

class TransactionModel {

    static get(id){
        return knex('transactions')
        .where('idaccountfrom', id)
        .orWhere('idaccountto', id);
    }

    static save(data){
        return knex
        .raw('INSERT INTO transactions (idAccountFrom, idAccountTo, amount, datetime, status) VALUES (' + data.idAccountFrom +', ' + data.idAccountTo + ', ' + data.amount + ', to_timestamp(' + data.date + '), ?)', ['A']);
    }

    static checkSameTransfer(data){
        return knex('transactions')
        .where('idaccountfrom', data.idAccountFrom)
        .andWhere('idaccountto', data.idAccountTo)
        .andWhere('amount', data.amount)
        .andWhereRaw(`datetime > NOW() - '2 minutes'::INTERVAL`); 

    }

    
    static updateTimeStamp(id, timestamp){
        return knex
        .raw('UPDATE transactions SET datetime = to_timestamp(?) WHERE id = ?', [timestamp, id]);
    }

}

module.exports = TransactionModel;
