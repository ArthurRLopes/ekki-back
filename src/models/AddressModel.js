const { knex } = require('../config/db');

class AddressModel {

    static save(data){
        return knex
        .from('addresses')
        .insert(data);
    }

    static getByUser(id){
        return knex
        .from('addresses')
        .where('addresses.id', id);
    }

    static get(data){
        return knex
        .from('addresses')
        .where({
            id: data.addressId,
            userId: data.UserId
        });
    }

    static delete(id){
        return knex
        .from('addresses')
        .where('id', id)
        .del();
    }

    static update(id, data){
        return knex('addresses')
        .where('id', id)
        .update(data);
    }

}

module.exports = AddressModel;