const CreditCardService = require('../services/CreditCardService');

class CreditCardsController {

    static async getCreditCardsByAccountId (req, res){
        const rows = await CreditCardService.getByAccountId(req.params.accountId);
        res.send({success: true, data: rows});
    }

    static async getCreditCard(req, res){
        const row = await CreditCardService.get(req.params.cardId);
        res.send({success: true, data: row});
    }

    static async updateCreditCard (req, res){
        const result = await CreditCardService.update(req.params.cardId, req.body);
        res.send({success: true, data: result});
    }

    static async saveCreditCard (req, res){
        const result = await CreditCardService.save(req.body);
        res.send({success: true, data: result});
    }

    static async deleteCreditCard (req, res){
        const result = await CreditCardService.delete(req.params.cardId);
        res.send({success: true, data: result});
    }

}

module.exports = CreditCardsController;