const UserService = require('../services/UserService');

class UserController {

    static async saveUser(req, res){

        const user = req.body;
        const result = await UserService.save(user);
        res.send({success: true, data: result});
    }

    static async getUser(req, res){
        const row = await UserService.get(req.params.userId);

        res.send({success: true, data: row});
    }

    static async authentication(req, res){
        const result = await UserService.auth(req.query);
        if (result !== false){
            res.send({success: true, data: result})
        }

        res.send({success: false});
    }

    static async getUsers(req, res){
        const rows = await UserService.getUsers(req.query.id);
        res.send({success: true, data: rows});
    }

}

module.exports = UserController;