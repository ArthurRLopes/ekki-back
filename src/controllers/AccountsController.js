const AccountService = require('../services/AccountService');

class AccountsController {

    static async saveAccount(req, res){
        const result = await AccountService.save(req.body);
        res.send({success: true, data: result});
    }

    static async getAccount(req, res){
        const row = await AccountsController.get(req.params.accountId);
        res.send({success: true, data: row});
    }

    static async getAccountsByUserId(req, res){
        const rows = await AccountsController.getByUser(req.params.userId);
        res.send({success: true, data: rows});
    }

    static async updateBalance(req, res){
        const result = await AccountService.update(req.params.accountId, req.body);
        res.send({success: true, data: result});
    }

    static async deleteAccount(req, res){
        const result = await AccountService.delete(req.params.accountId);
        res.send({success: true, data: result});
    }

}

module.exports = AccountsController;

