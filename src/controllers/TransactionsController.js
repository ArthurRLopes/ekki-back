const TransactionService = require('../services/TransactionService');

class TransactionController {

    static async getTransactionsByAccount(req, res){
        const rows = await TransactionService.getByAccount(req.params.idAccount);
        res.send({success: true, data: rows});
    }

    static async saveTransaction(req, res){
        const result = await TransactionService.save(req.body);
        res.send({success: true, result});
    }

}

module.exports = TransactionController;