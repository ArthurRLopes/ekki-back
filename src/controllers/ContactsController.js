const ContactService = require('../services/ContactService');

class ContactsController {

    static async getContactsByUserId (req, res){
        console.log(req.query);
        const rows = await ContactService.getByUserId(req.params.userId);
        res.send({success: true, data: rows});
    }

    static async getContact (req, res){
        const row = await ContactService.get(req.params.userId);
        res.send({success: true, data: rows});
    }

    static async saveContact (req, res){
        const result = await ContactService.save(req.body);
        res.send({success: true, data: result});
    }

    static async updateContact (req, res){
        const result = await ContactService.update(req.params.contactId, req.body);
        res.send({success: true, data: result});
    }

    static async deleteContact (req, res){
        const result = await ContactService.delete(req.params.contactId);
        res.send({success: true, data: result});
    }

}

module.exports = ContactsController;