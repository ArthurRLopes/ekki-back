const AddressService = require('../services/AddressService');

class AddressesController {

    static async saveAddress(req, res){
        const result = await AddressService.save(req.body);
        res.send({success: true, data: result});
    }

    static async getAddressesByUserId(req, res){
        const rows = await AddressService.getByUser(req.params.userId);
        res.send({success: true, data: rows});
    }

    static async getAddressById(req, res){
        const row = await AddressService.get(req.params);
        res.send({success: true, data: row});
    }

    static async deleteAddress(req, res){
        const result = await AddressService.delete(req.params.addressId);
        res.send({success: true, data: result});
    }

    static async updateAddress(req, res){
        const result = await AddressService.update(req.params.addressId, req.body);
        res.send({success: true, data: result});
    }

}

module.exports = AddressesController;