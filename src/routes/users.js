const express = require('express');
const UserController = require('../controllers/UserController');

const router = express.Router({mergeParams: true});

router.post('/', UserController.saveUser);
router.get('/get/:userId', UserController.getUser);
router.get('/', UserController.getUsers);
router.get('/auth/', UserController.authentication);

module.exports = router;