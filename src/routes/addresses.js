const express = require('express');

const AddressesController = require('../controllers/AddressController');
const router = express.Router({mergeParams: true});

router.post('/', AddressesController.saveAddress);
router.get('/:userId', AddressesController.getAddressesByUserId);
router.delete('/:addressId', AddressesController.deleteAddress);
router.put('/:addressId', AddressesController.updateAddress);
router.get('/:userId/:addressId', AddressesController.getAddressById);

module.exports = router;
