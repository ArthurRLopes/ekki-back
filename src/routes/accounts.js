const express = require('express');

const AccountsController = require('../controllers/AccountsController');

const router = express.Router({mergeParams: true});

router.post('/', AccountsController.saveAccount);
router.get('/:accountId', AccountsController.getAccount);
router.get('/allByuser/:userId', AccountsController.getAccountsByUserId);
router.put('/:accountId', AccountsController.updateBalance);
router.delete('/:accountId', AccountsController.deleteAccount);

module.exports = router;