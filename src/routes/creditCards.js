const express = require('express');

const CreditCardsController = require('../controllers/CreditCardsController');

const router = express.Router({mergeParams: true});

router.get('/cards/:accountId', CreditCardsController.getCreditCardsByAccountId);
router.get('/:cardId', CreditCardsController.getCreditCard);
router.put('/:cardId', CreditCardsController.updateCreditCard);
router.post('/:accountId', CreditCardsController.saveCreditCard);
router.delete('/:cardId', CreditCardsController.deleteCreditCard);

module.exports = router;