const express = require('express');

const TransactionsController = require('../controllers/TransactionsController');

const router = express.Router({mergeParams: true});

router.get('/:idAccount', TransactionsController.getTransactionsByAccount);
router.post('/', TransactionsController.saveTransaction);

module.exports = router;