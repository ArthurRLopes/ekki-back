const express = require('express');

const ContactsController = require('../controllers/ContactsController');

const router = express.Router({mergeParams: true});

router.get('/:userId', ContactsController.getContactsByUserId);
router.get('/:userId/:contactId', ContactsController.getContact);
router.post('/:userId', ContactsController.saveContact);
router.put('/:contactId', ContactsController.updateContact);
router.delete('/:contactId', ContactsController.deleteContact);

module.exports = router;