let config = {
    client: 'pg',
    connection: {
        host: 'localhost',
        user: 'postgres',
        password: '13arpes6',
        database: 'Ekki',
        timezone: 'UTC',
        multipleStatements: true
    },
    pool: {
        min: parseInt(0, 10),
        max: parseInt(1, 10)
    }
}

const db = require('knex')(config);

module.exports = {
    knex: db,
    raw: db.raw
}