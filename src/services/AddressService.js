const AddressModel = require('../models/AddressModel');

class AddressService {
    
    static async save(data){
        return await AddressModel.save(data);
    }

    static async getByUser(userId){
        return await AddressModel.getByUser(userId);
    }

    static async get(data){
        return await AddressModel.get(data);
    }

    static async delete(addressId){
        return await AddressModel.delete(addressId);
    }

    static async update(addressId, data){
        return await AddressModel.update(addressId, data);
    }

}

module.exports = AddressService;