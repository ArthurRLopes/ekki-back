const ContactModel = require('../models/ContactModel');

class ContactService {

    static async getByUserId(id){
        return await ContactModel.getByUser(id);
    }

    static async get(id){
        return await ContactModel.get(id);
    }

    static async save(data){
        return await ContactModel.save(data);
    }

    static async update(id, data){
        return await ContactModel.update(id, data);
    }

    static async delete(id){
        return await ContactModel.delete(id);
    }

}

module.exports = ContactService;