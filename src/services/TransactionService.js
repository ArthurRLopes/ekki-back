const TransactionModel = require('../models/TransactionModel');
const UserModel = require('../models/UserModel');
const CreditCardModel = require('../models/CreditCardModel');

class TransactionService {
    
    static async getByAccount(id){
        return await TransactionModel.get(id);
    }

    static async changeBalance(idAccountFrom, idAccountTo, amount){
        let operation = '-';
        await UserModel.update(idAccountFrom, amount, operation);
        operation = '+';
        await UserModel.update(idAccountTo, amount, operation);
    }

    static async save(data){
        
        let check = await TransactionModel.checkSameTransfer(data.params);
        data.params.date = Date.now() / 1000;
        if (check.length > 0){
            return await TransactionModel.updateTimeStamp(check[0].id, data.params.date)
        } else {

            let amount = data.params.amount;
            let balance = await UserModel.getBalance(data.params.idAccountFrom);
            
            if (amount > balance[0].balance){

                let creditCard = await CreditCardModel.getByAccount(data.params.idAccountFrom);

                if (creditCard.length > 0){
                    this.changeBalance(data.params.idAccountFrom, data.params.idAcountTo, data.params.amount);
                    return {
                        status: 'no-funds-with-credit-card',
                        rows: await TransactionModel.save(data.params)
                    }    
                } else {
                    return {
                        status: 'no-funds-with-no-credit-card'
                    }
                }

            } else {
                this.changeBalance(data.params.idAccountFrom, data.params.idAccountTo, data.params.amount)
                return await TransactionModel.save(data.params);
            }
        }

            // if (creditCard.length > 0) {

            //     let operation = '-';
            //     await UserModel.update(data.params.idAccountFrom, data.params, operation);
            //     operation = '+';
            //     await UserModel.update(data.params.idAccountFrom, data.params, operation);

            //     let rows = await TransactionModel.save(data.params);

            //     if (amount > balance[0].balance){
            //         return {
            //             status: 'no-funds-with-credit-card',
            //             rows: rows
            //         }
            //     } else {
            //         return rows;
            //     }

            // } else {
            //     return {
            //         status: 'no-funds-with-no-credit-card'
            //     }
            // }
        
        
        // if (amount > balance[0].balance){
            
        //     let creditCard = await CreditCardModel.getByAccount(data.params.idAccountFrom);
            
        //     if (creditCard.length > 0){

        //         operation = '-';

        //         resultBalanceChangeAccountFrom = await UserModel.update(data.params.idAccountFrom, data.params, operation);
                
        //         if (resultBalanceChangeAccountFrom){
        //             operation = '+';
                    
        //             resultBalanceChangeAccountTo = await UserModel.update(data.params.idAccountTo, data.params, operation);
                    
        //             if (resultBalanceChangeAccountTo){

        //                 data.params.date = Date.now() / 1000;
                        
        //                 check = await TransactionModel.checkSameTransfer(data.params);
                        
        //                 if (check.length > 0) {
        //                     rows = await TransactionModel.updateTimeStamp(check[0].id, data.params.date)
        //                 } else {
        //                     rows = await TransactionModel.save(data.params)
        //                 }
        //                 return {
        //                     status: "no-funds-with-credit-card",
        //                     rows: rows
        //                 }
        //             }
        //         }
        //     }

        //     return {
        //         status: "no-funds-with-no-credit-card"
        //     }
        // }

        // operation = '-';

        // resultBalanceChangeAccountFrom = await UserModel.update(data.params.idAccountFrom, data.params, operation);
        
        // if (resultBalanceChangeAccountFrom){
        //     operation = '+';
            
        //     resultBalanceChangeAccountTo = await UserModel.update(data.params.idAccountTo, data.params, operation);
            
        //     if (resultBalanceChangeAccountTo){
        //         data.params.date = Date.now() / 1000;

        //         check = await TransactionModel.checkSameTransfer(data.params);
                
        //         if (check.length > 0) {
        //             rows = await TransactionModel.updateTimeStamp(check[0].id, data.params.date)
        //         } else {
        //             rows = await TransactionModel.save(data.params)
        //         }

        //         return rows;
        //     }

        // }

    }

}

module.exports = TransactionService;