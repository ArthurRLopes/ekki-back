const UserModel = require('../models/UserModel');
const bcrypt = require('bcrypt');

class UserService {

    static async save(data){
        
        const salt = bcrypt.genSaltSync(10);

        data.password = bcrypt.hashSync(data.password, salt);

        return await UserModel.save(data);
    }

    static async get(id){
        const user = await UserModel.get(id);
        return user[0];
    }

    static async auth(data){
        let user = await UserModel.auth(data.email);
        
        const check = await bcrypt.compare(data.password, user[0].password);

        if (check){
            return user[0];
        }

        return false;
    }

    static async getUsers(id){
        const users = await UserModel.getUsers(id);

        return users;
    }

}

module.exports = UserService;