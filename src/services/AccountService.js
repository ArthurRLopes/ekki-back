const AccountModel = require('../models/AccountModel');

class AccountService {

    static async save(data) {
        return await AccountModel.save(data);
    }
    
    static async get(id){
        return await AccountModel.get(id);
    }

    static async getByUser(id){
        return await AccountModel.getByUser(id);
    }

    static async update(id, data){
        return await AccountModel.update(id, data);
    }

    static async delete(id){
        return await AccountModel.delete(id);
    }
}

module.exports = AccountService;