const CreditCardModel = require('../models/CreditCardModel');

class CreditCardService {

    static async getByAccountId(id){
        return await CreditCardModel.getByAccount(id);
    }

    static async get(data){
        return await CreditCardModel.get(data);
    }

    static async update(id, data){
        return await CreditCardModel.update(id, data);
    }
    
    static async save(data){
        return await CreditCardModel.save(data);
    }

    static async delete(id){
        return await CreditCardModel.delete(id);
    }

}

module.exports = CreditCardService;