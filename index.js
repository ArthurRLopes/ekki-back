const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3001;
const cors = require('cors');
app.use(cors());
const userRoutes = require('./src/routes/users');
const addressRoutes = require('./src/routes/addresses');
const accountRoutes = require('./src/routes/accounts');
const transactionRoutes = require('./src/routes/transactions');
const creditCardRoutes = require('./src/routes/creditCards');
const contactsRoutes = require('./src/routes/contacts');

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

app.use('/users', userRoutes);
app.use('/addresses', addressRoutes);
app.use('/accounts', accountRoutes);
app.use('/transactions', transactionRoutes);
app.use('/creditCards', creditCardRoutes);
app.use('/contacts', contactsRoutes);

app.listen(port, () => {
     console.log('App Running on Port: ' + port);
})